let log = require('@dosarrest/loopback-component-logger')('perspective-main/boot');
// let cascadeDelete = require('loopback-cascade-delete-mixin/cascade-delete');
class Boot {
  constructor(app, done) {
    let me = this;
    me.app = app;
    me.done = done;
    me.init();
  }
  init() {
    let me = this;
    let done = me.done;
    let app = me.app;
    let ACL = app.registry.getModelByType('ACL');
    let MainNavigation = app.registry.getModelByType('MainNavigation');
    try {
      ACL.findOrCreate({
        model: MainNavigation.definition.name,
        accessType: 'READ',
        principalType: 'ROLE',
        principalId: '$everyone',
        permission: 'ALLOW',
        property: '*'
      });
    } catch (err) {
      log.debug(err);
      log.info('@extlb/module-notifications not installed');
    }
    done();
  }
}
module.exports = Boot;
