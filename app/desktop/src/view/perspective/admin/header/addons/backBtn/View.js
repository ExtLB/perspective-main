Ext.define('Client.view.perspective.admin.header.addons.backBtn.View', {
  extend: 'Ext.Container',
  controller: {type: 'perspective.admin.header.addons.backBtn'},
  viewModel: {type: 'perspective.admin.header.addons.backBtn'},
  padding: 5,
  items: [{
    xtype: 'button',
    ui: 'round raised',
    bind: {
      tooltip: '{"per-main:BACK":translate}'
    },
    reference: 'backBtn',
    handler: 'onBackBtn',
    iconCls: 'x-fa fa-arrow-left',
  }]
});
Client.view.perspective.admin.header.addons.backBtn.View.addStatics({
  order: 1
});
