Ext.define('Client.view.perspective.admin.header.addons.backBtn.ViewController', {
  extend: 'Ext.app.ViewController',
  alias: 'controller.perspective.admin.header.addons.backBtn',

  onBackBtn: (button) => {
    let Navigation = Client.app.getController('Navigation');
    Navigation.gotoPerspective('main');
  },

    // setTimeout(() => {
    //   let perspective = view.up('perspective\\.main');
    //   let mainVM = perspective.getViewModel();
    //   let user = mainVM.get('user');
    //   // let perspective = Ext.Viewport.down('perspective\\.main');
    //   console.log(view, perspective, user);
    // }, 10);
});
