Ext.define('Client.view.perspective.admin.header.addons.backBtn.ViewModel', {
  extend: 'Ext.app.ViewModel',
  alias: 'viewmodel.perspective.admin.header.addons.backBtn',
  data: {
    user: {},
    isAdmin: false,
  }
});
