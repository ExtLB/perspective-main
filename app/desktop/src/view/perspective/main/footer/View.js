Ext.define('Client.view.perspective.main.footer.View', {
	extend: 'Ext.Toolbar',
	xtype: 'perspective.main.footer',
	cls: 'perspective-main-footer',

	items: [
		{
			xtype: 'container',
			cls: 'perspective-main-footertext',
      bind: {
        html: '{"per-main:FOOTER":translate}'
      }
		},
//		'->',
//		{
//			xtype: 'button',
//			ui: 'footerbutton',
//			iconCls: 'x-fa fa-automobile'
//		}
	]
});
