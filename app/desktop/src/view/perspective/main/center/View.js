Ext.define('Client.view.perspective.main.center.View', {
	extend: 'Ext.Container',
	xtype: 'perspective.main.center',
	cls: 'perspective-main-center',
	layout: 'card'
});
