Ext.define('Client.view.perspective.main.navigation.View', {
	extend: 'Ext.Panel',
	xtype: 'perspective.main.navigation',
	controller: "perspective.main.navigation",
	cls: 'perspective-main-navigation',
	layout: 'fit',
	tbar: {xtype: 'perspective.main.nav.top', bind: { height: '{headerview_height}' }},
	items: [
		{
			xtype: 'perspective.main.nav.menu',
			reference: 'menuview',
			bind: {width: '{navview_width}'},
			listeners: {
				selectionchange: "onMenuViewSelectionChange"
			}
		}
	],
	bbar: {xtype: 'perspective.main.nav.bottom', bind: {height: '{bottomview_height}', hidden: '{!auth.enabled}'}}
});
