Ext.define('Client.view.perspective.main.nav.menu.View', {
	extend: 'Ext.list.Tree',
	xtype: 'perspective.main.nav.menu',
	ui: 'perspective-main-navigation',
	requires: [
		'Ext.data.TreeStore',
	],
	scrollable: true,
	bind: {
		store: '{menu}',
		micro: '{navCollapsed}'
	},
	expanderFirst: false,
	expanderOnly: false
});
