Ext.define('Client.view.perspective.main.nav.top.View', {
	extend: 'Ext.Toolbar',
	xtype: 'perspective.main.nav.top',
	cls: 'perspective-main-nav-top',
	shadow: false,
	items: [
		{
			xtype: 'container',
			cls: 'perspective-main-nav-toptext',
			bind: {
				html: '{name:translate}',
				hidden: '{navCollapsed}'
			}
		}, '->', {
			xtype: 'button',
			ui: 'perspective-main-nav-topbutton',
      bind: {
			  tooltip: '{"per-main:TOP_MENU_BTN_TOOLTIP":translate}'
      },
			reference: 'navtoggle',
			handler: 'onTopViewNavToggle',
			iconCls: 'x-fa fa-bars'
		}
	]
});
