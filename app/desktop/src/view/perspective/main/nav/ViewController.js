Ext.define('Client.view.main.nav.NavViewController', {
	extend: "Ext.app.ViewController",
	alias: "controller.perspective.main.navigation",
  requires: [
    'Ext.Ajax',
    'Ext.util.Cookies'
  ],

	// initViewModel: function(vm) {},

	onTopViewNavToggle: function (btn) {
		var vm = this.getViewModel();
		vm.set('navCollapsed', !vm.get('navCollapsed'));
		btn.setIconCls((vm.get('navCollapsed')?'logo-big':'x-fa fa-bars'));
	},

	onMenuViewSelectionChange: function(tree, node) {
		if (!node) {
				return;
		}
		this.fireViewEvent("select", node);
	},

	onBottomViewlogout: function () {
	  let me = this;
    localStorage.setItem("LoggedIn", false);
    Client.app.getController('Authentication').logout();
		// this.getView().destroy();
		// Ext.Viewport.add([{ xtype: 'loginview'}]);
	},
});
