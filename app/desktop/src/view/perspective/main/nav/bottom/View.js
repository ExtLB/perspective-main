Ext.define('Client.view.perspective.main.nav.bottom.View', {
	extend: 'Ext.Toolbar',
	xtype: 'perspective.main.nav.bottom',
	cls: 'perspective-main-nav-bottom',
	shadow: false,
	items: [
		{
			xtype: 'button',
			ui: 'perspective-main-nav-bottombutton',
      padding: '0 0 0 6px',
			iconCls: 'x-fa fa-angle-double-left',
      bind: {
        text: '{"per-main:LOGOUT":translate}',
        tooltip: '{"per-main:LOGOUT":translate}',
      },
			handler: 'onBottomViewlogout'
		}
	]
});
