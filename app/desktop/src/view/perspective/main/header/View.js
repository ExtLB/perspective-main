Ext.define('Client.view.perspective.main.header.View', {
	extend: 'Ext.Toolbar',
	xtype: 'perspective.main.header',
	cls: 'perspective-main-header',
  requires: [
    'Ext.XTemplate',
    'Ext.dataview.plugin.ListPaging',
    'Ext.dataview.List',
    'Ext.menu.Menu',
    'Ext.Img'
  ],
	items: [
		{
			xtype: 'container',
			cls: 'perspective-main-headertext',
			bind: { html: '{heading:mainHeader}' }
		}, '->', {
	    xtype: 'container',
      reference: 'headerBtns',
      padding: 5,
      items: [],
    }
	]
});
