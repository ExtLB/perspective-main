Ext.define('Client.view.perspective.main.View', {
	extend: 'Ext.Container',
	xtype: 'perspective.main',
	controller: {type: 'perspective.main'},
	viewModel: {type: 'perspective.main'},
  requires: [
    'Ext.layout.Fit'
  ],
	layout: 'fit',
  listeners: {
	  initialize: 'onMainViewInitialize'
  },
	items: [
		{ xtype: 'perspective.main.navigation',    reference: 'navview',    docked: 'left',   bind: {width:  '{navview_width}'}, listeners: { select: "onMenuViewSelectionChange"} },
		{ xtype: 'perspective.main.header', reference: 'headerview', docked: 'top',    bind: {height: '{headerview_height}', hidden: '{!showHeader}'} },
		{ xtype: 'perspective.main.footer', reference: 'footerview', docked: 'bottom', bind: {height: '{footerview_height}', hidden: '{!showFooter}'} },
		{ xtype: 'perspective.main.center', reference: 'centerview' },
	]
});
